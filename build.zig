const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    const mode = b.standardReleaseOptions();
    const exe = b.addExecutable("journal", null);
    exe.setBuildMode(mode);
    exe.linkSystemLibrary("c");

    const source_files = [_][]const u8{"src/journal/main.c"};

    for (source_files) |source| {
        exe.addCSourceFile(source, [_][]const u8{"-Wall"});
    }

    const zig_sources = [_][]const u8{"src/journal/journal.zig"};

    for (zig_sources) |source| {
        const obj = b.addObject("journal", source);
        obj.linkSystemLibrary("c");
        exe.addObject(obj);
    }

    // enable us to run the main journal executable via
    // the build system
    const run_cmd = exe.run();
    const run_step = b.step("run", "Run journal");
    run_step.dependOn(&run_cmd.step);

    b.default_step.dependOn(&exe.step);
    b.installArtifact(exe);
}
