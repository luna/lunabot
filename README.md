# lunabot

utils for me, as a robot girl

source is open, data is closed, this is for my own personal use

## depedencies

 - a robot girl to use those utils
 - the zig compiler https://ziglang.org

```
zig build install --prefix ~/.local/
```

## using

there's currently one utility for me which is journal.

`journal emotion happy`, etc. list of topics can be found via `journal list`.
