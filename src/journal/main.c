#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "journal.h"

#define TOPICS 10

// go through all elements of argv[2] and beyond
// and join them together in a string
char *extract_handler_msg(int argc, char **argv)
{
    int cur = 0;

    // allocate an empty slot to start with.
    char *res = malloc(cur);

    // iterate through each argument
    for(int i = 2; argv[i] != NULL; i++)
    {
        // catch it, get its length
        char *word = argv[i];
        int wordlen = strlen(word);

        // increase the size of our current things by
        // arg length + 1 (for a whitespace)
        cur += wordlen;
        res = realloc(res, cur + 1);

        // only prepend our whitespace if we AREN'T
        // at the first argument.
        if(i != 2)
        {
            res[cur - wordlen] = ' ';
            cur++;
        }

        // merge our current results with the current arg
        for(int j = 0; j < wordlen; j++)
        {
            res[cur - wordlen + j] = word[j];
        }
    }

    return res;
}

int main(int argc, char** argv)
{
    if(argc < 2)
    {
        printf("usage: %s topic message\n", argv[0]);
        return 0;
    }

    char *topic = argv[1];

    // topics are injected at compile time
    // yes, i'm keeping track of those. don't shame me :)))))
    const char* topics[TOPICS] = {
        "emotion", "lust", "orgasm"
    };

    // default handling by journal_write_topic is marked
    // as the NULL values in this array.
    void (*handlers[])(int, char*) = {
        NULL, NULL, NULL
    };

    if(STREQ(topic, "version"))
    {
        printf("lunabot journal v%s\n", JOURNAL_VERSION);
        return 0;
    }

    // list all topics when arg1 is list
    if(STREQ(topic, "list"))
    {
        for(int i = 0; topics[i] != NULL; i++)
            printf("%s ", topics[i]);
        printf("\n");

        return 0;
    }

    // go through each topic to find which one we want
    // to push the current entry to
    for(int i = 0; topics[i] != NULL; i++)
    {
        const char* cur_topic = topics[i];

        if(strcmp(topic, cur_topic) == 0)
        {
            void (*fun_ptr)(int, char*) = handlers[i];

            int journal_file = journal_open(topic);
            if(journal_file == -1) {
                printf("failed to open journal file.\n");
                return 1;
            }

            char *handler_message = extract_handler_msg(argc, argv);
            printf("[%s] said '%s'\n", topic, handler_message);

            if(fun_ptr == NULL) {
                journal_write_topic(journal_file, topic, handler_message);
            } else {
                fun_ptr(journal_file, handler_message);
            }

            journal_close(journal_file);
            free(handler_message);

            printf("done!\n");
            return 0;
        }
    }

    printf("topic %s not found\n", topic);
    return 0;
}

