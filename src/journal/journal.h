#ifndef __JOURNAL_H__
#define __JOURNAL_H__

#include <stdio.h>
#include <string.h>

#define JOURNAL_VERSION "0.0.1"

#define STREQ(a, b) strcmp(a, b) == 0

int journal_open(char *topic);
void journal_write(int, char* message);
void journal_write_topic(int, char *topic, char *message);
void journal_close(int);

#endif
